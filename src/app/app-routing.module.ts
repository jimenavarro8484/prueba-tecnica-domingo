import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { ContactSettingsComponent } from './contact-settings/contact-settings.component';

const routes: Routes = [
  { path: '', component: ContactComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'contact-settings', component: ContactSettingsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
