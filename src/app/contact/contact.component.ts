import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  /* data of message to send */
  data = {
    name: '',
    email: '',
    subject: '',
    content: ''
  }

  constructor(public router: Router) { 
    localStorage.clear();
  }

  ngOnInit() {
  }
  
      /**
     * Save message and go to contact settings
     * @param  null
     * @return null
     */
  sendMessage() {
    // console.log(this.data);
    localStorage.setItem('messageData', JSON.stringify(this.data));
    this.router.navigate(['/contact-settings']);
  }

}
