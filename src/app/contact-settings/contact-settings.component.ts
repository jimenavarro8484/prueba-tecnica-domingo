import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact-settings',
  templateUrl: './contact-settings.component.html',
  styleUrls: ['./contact-settings.component.scss']
})
export class ContactSettingsComponent implements OnInit {
    /* declared to data of message saved on localstorage */
  data: any = [];

    /* parse json from data */
  info = {
    name: '',
    email: '',
    subject: '',
    content: ''
  };

    /* to know if there is message or not */
  emptyItems: boolean;

    /* to know if the user is admin or not */
  isAdmin: boolean;

    /* let view to edit message */
  editingData: boolean;

  /* modal result */
  closeResult = '';

  constructor(private modalService: NgbModal) { 
    this.gettingData();
  }

    /* getting data to show */
  gettingData() {
    this.data = localStorage.getItem('messageData');
    const dataEmpty = this.isEmpty(this.data);
    if(dataEmpty === false) {
      console.log('no esta vacio');
      this.emptyItems = false;
      this.info = JSON.parse(this.data);
      console.log('data: ', this.info);
    } else {
      console.log('si esta vacio');
      this.emptyItems = true;
    }
  }

  ngOnInit() {
    this.isAdmin = false;
    this.editingData = false;
  }
  /* cancel delete message */
  cancelEdit() {
    this.editingData = false;
  }

    /* delete all data from message */
  deletMessage() {
    this.info = {
      name: '',
      email: '',
      subject: '',
      content: ''
    };
    localStorage.clear();
    this.gettingData();
  }

    /* changind variable to edit data message */
  editMessage() {
    this.editingData = true;
  }

    /* to know if there is data on local storage from message */
  isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop)){
          return false;
        } else {
          return true;
        }
    }
  }

    /* to know who rol the user is */
  onChange(event) {
    console.log(event.target.value);
    const user = event.target.value;
    if(user == 'admin') {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }

    /* to save changes of message */
  saveChanges() {
    this.isAdmin = true;
    this.editingData = false;
    localStorage.setItem('messageData', JSON.stringify(this.info));
  }

    /* open de modal to delete message */
   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
      this.deletMessage();
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason);
    });
  } 

    /* to know the way the modal was close */
   private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  } 

}
